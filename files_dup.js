var files_dup =
[
    [ "controller_driver.py", "controller__driver_8py.html", [
      [ "controller_driver.ClosedLoop", "classcontroller__driver_1_1ClosedLoop.html", "classcontroller__driver_1_1ClosedLoop" ]
    ] ],
    [ "encoder_driver.py", "encoder__driver_8py.html", [
      [ "encoder_driver.Encoder", "classencoder__driver_1_1Encoder.html", "classencoder__driver_1_1Encoder" ]
    ] ],
    [ "finalprojectmainpage.py", "finalprojectmainpage_8py.html", null ],
    [ "fp_IMU_driver.py", "fp__IMU__driver_8py.html", [
      [ "fp_IMU_driver.BNO055", "classfp__IMU__driver_1_1BNO055.html", "classfp__IMU__driver_1_1BNO055" ]
    ] ],
    [ "fp_main.py", "fp__main_8py.html", "fp__main_8py" ],
    [ "fp_motor_driver.py", "fp__motor__driver_8py.html", [
      [ "fp_motor_driver.Motor", "classfp__motor__driver_1_1Motor.html", "classfp__motor__driver_1_1Motor" ]
    ] ],
    [ "fp_panel_driver.py", "fp__panel__driver_8py.html", [
      [ "fp_panel_driver.TouchPanel", "classfp__panel__driver_1_1TouchPanel.html", "classfp__panel__driver_1_1TouchPanel" ]
    ] ],
    [ "fp_task_controller.py", "fp__task__controller_8py.html", "fp__task__controller_8py" ],
    [ "fp_task_data.py", "fp__task__data_8py.html", [
      [ "fp_task_data.Task_Data", "classfp__task__data_1_1Task__Data.html", "classfp__task__data_1_1Task__Data" ]
    ] ],
    [ "fp_task_IMU.py", "fp__task__IMU_8py.html", "fp__task__IMU_8py" ],
    [ "fp_task_motor.py", "fp__task__motor_8py.html", [
      [ "fp_task_motor.Task_Motor", "classfp__task__motor_1_1Task__Motor.html", "classfp__task__motor_1_1Task__Motor" ]
    ] ],
    [ "fp_task_panel.py", "fp__task__panel_8py.html", "fp__task__panel_8py" ],
    [ "fp_task_user.py", "fp__task__user_8py.html", "fp__task__user_8py" ],
    [ "Hwk2and3main.py", "Hwk2and3main_8py.html", null ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main_lab02.py", "main__lab02_8py.html", "main__lab02_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "mainpagelab4.py", "mainpagelab4_8py.html", null ],
    [ "motor_driver.py", "motor__driver_8py.html", [
      [ "motor_driver.DRV8847", "classmotor__driver_1_1DRV8847.html", "classmotor__driver_1_1DRV8847" ],
      [ "motor_driver.Motor", "classmotor__driver_1_1Motor.html", "classmotor__driver_1_1Motor" ]
    ] ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_encoder.py", "task__encoder_8py.html", [
      [ "task_encoder.Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "task_motor.py", "task__motor_8py.html", [
      [ "task_motor.Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_user.py", "task__user_8py.html", "task__user_8py" ]
];