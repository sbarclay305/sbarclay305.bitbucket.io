var annotated_dup =
[
    [ "controller_driver", null, [
      [ "ClosedLoop", "classcontroller__driver_1_1ClosedLoop.html", "classcontroller__driver_1_1ClosedLoop" ]
    ] ],
    [ "encoder_driver", null, [
      [ "Encoder", "classencoder__driver_1_1Encoder.html", "classencoder__driver_1_1Encoder" ]
    ] ],
    [ "fp_IMU_driver", null, [
      [ "BNO055", "classfp__IMU__driver_1_1BNO055.html", "classfp__IMU__driver_1_1BNO055" ]
    ] ],
    [ "fp_motor_driver", null, [
      [ "Motor", "classfp__motor__driver_1_1Motor.html", "classfp__motor__driver_1_1Motor" ]
    ] ],
    [ "fp_panel_driver", null, [
      [ "TouchPanel", "classfp__panel__driver_1_1TouchPanel.html", "classfp__panel__driver_1_1TouchPanel" ]
    ] ],
    [ "fp_task_controller", null, [
      [ "Task_Controller", "classfp__task__controller_1_1Task__Controller.html", "classfp__task__controller_1_1Task__Controller" ]
    ] ],
    [ "fp_task_data", null, [
      [ "Task_Data", "classfp__task__data_1_1Task__Data.html", "classfp__task__data_1_1Task__Data" ]
    ] ],
    [ "fp_task_IMU", null, [
      [ "Task_IMU", "classfp__task__IMU_1_1Task__IMU.html", "classfp__task__IMU_1_1Task__IMU" ]
    ] ],
    [ "fp_task_motor", null, [
      [ "Task_Motor", "classfp__task__motor_1_1Task__Motor.html", "classfp__task__motor_1_1Task__Motor" ]
    ] ],
    [ "fp_task_panel", null, [
      [ "Task_panel", "classfp__task__panel_1_1Task__panel.html", "classfp__task__panel_1_1Task__panel" ]
    ] ],
    [ "fp_task_user", null, [
      [ "Task_User", "classfp__task__user_1_1Task__User.html", "classfp__task__user_1_1Task__User" ]
    ] ],
    [ "motor_driver", null, [
      [ "DRV8847", "classmotor__driver_1_1DRV8847.html", "classmotor__driver_1_1DRV8847" ],
      [ "Motor", "classmotor__driver_1_1Motor.html", "classmotor__driver_1_1Motor" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "task_motor", null, [
      [ "Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_user", null, [
      [ "Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ]
];